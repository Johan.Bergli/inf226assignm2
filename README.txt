Part 2A

Made the secret key a little bit longer.

The users and their passwords is stored as a dictionary in the
app.py file. This should be store encrypted in a database.
Used haslib to encrypt passwords with the username as salt.
The reason i used haslib is because it offers a one way encryption
which is better when storing passwords.

Added a way to log you out.

Tried check the input from the user to make sure it is not
malicious code. This is done by the legalMessage class.
The check is very strict so there is not many legal characters.

To save confidentiality, a person can only see their own messages or messages
sent to them.
