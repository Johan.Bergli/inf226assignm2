from http import HTTPStatus
from flask import Flask, abort, request, send_from_directory, make_response, render_template, session
from werkzeug.datastructures import WWWAuthenticate
import flask
from login_form import LoginForm
from json import dumps, loads
from base64 import b64decode
import sys
import apsw
from apsw import Error
from pygments import highlight
from pygments.lexers import SqlLexer
from pygments.formatters import HtmlFormatter
from pygments.filters import NameHighlightFilter, KeywordCaseFilter
from pygments import token;
from threading import local
from markupsafe import escape
from datetime import timedelta
from LegalMessage import *
from Encrypt import *

tls = local()
inject = "'; insert into messages (sender,message) values ('foo', 'bar');select '"
cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')
conn = None

# Set up app
app = Flask(__name__)
# The secret key enables storing encrypted session data in a cookie (make a secure random key for this!)
app.secret_key = '34d4d4b9e2df4e109def8bc4076b1eb6' #'mY s3kritz'
# Create sessions
# Strukturer koden



# Add a login manager to the app
import flask_login
from flask_login import login_required, login_user, logout_user, current_user
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"
login_manager.refresh_view = 'relogin'
login_manager.needs_refresh_message = (u"Session timedout, please re-login")
login_manager.needs_refresh_message_category = "info"

#Get password from database if the user exsitst
def get_password(userName):
    try:
        psw = None
        for row in c.execute("SELECT * FROM passwords "):
            if row[1] == userName:
                psw = row[2]
        if psw is None:
            return None
        return psw
    except:
        return None

#Compare password from database to password from input
def check_password(userName, userPassword):
    psw = get_password(userName) # psw = flase if user does not exist
    if (psw):
        return encryption(userName, userPassword) == psw

# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
class User(flask_login.UserMixin):
    pass

@app.route('/')
@app.route('/index.html')
@login_required
def index_html():
    return send_from_directory(app.root_path,
                        'index.html', mimetype='text/html')

# This method is called whenever the login manager needs to get
# the User object for a given user id
@login_manager.user_loader
def user_loader(user_id):
    try:
        for row in c.execute("SELECT * FROM passwords "):
            if row[1] == user_id:
                user = User()
                user.id = user_id
                return user
        return
    except:
        return

# This method is called to get a User object based on a request,
# for example, if using an api key or authentication token rather
# than getting the user name the standard way (from the session cookie)
@login_manager.request_loader
def request_loader(request):
    # Even though this HTTP header is primarily used for *authentication*
    # rather than *authorization*, it's still called "Authorization".
    auth = request.headers.get('Authorization')

    # If there is not Authorization header, do nothing, and the login
    # manager will deal with it (i.e., by redirecting to a login page)
    if not auth:
        return

    (auth_scheme, auth_params) = auth.split(maxsplit=1)
    auth_scheme = auth_scheme.casefold()
    if auth_scheme == 'basic':  # Basic auth has username:password in base64
        (uid,passwd) = b64decode(auth_params.encode(errors='ignore')).decode(errors='ignore').split(':', maxsplit=1)
        #print(f'Basic auth: {uid}:{passwd}')
        if check_password(uid, passwd):
            return user_loader(uid)
    elif auth_scheme == 'bearer': # Bearer auth contains an access token;
        # an 'access token' is a unique string that both identifies
        # and authenticates a user, so no username is provided (unless
        # you encode it in the token – see JWT (JSON Web Token), which
        # encodes credentials and (possibly) authorization info)
        #print(f'Bearer auth: {auth_params}')
        for row in c.execute("SELECT * FROM passwords "):
            if row[3] == auth_params:
                return user_loader(row[1])
    # For other authentication schemes, see
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication

    # If we failed to find a valid Authorized header or valid credentials, fail
    # with "401 Unauthorized" and a list of valid authentication schemes
    # (The presence of the Authorized header probably means we're talking to
    # a program and not a user in a browser, so we should send a proper
    # error message rather than redirect to the login page.)
    # (If an authenticated user doesn't have authorization to view a page,
    # Flask will send a "403 Forbidden" response, so think of
    # "Unauthorized" as "Unauthenticated" and "Forbidden" as "Unauthorized")
    abort(HTTPStatus.UNAUTHORIZED, www_authenticate = WWWAuthenticate('Basic realm=inf226, Bearer'))

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        if check_password(username, password):
            user = user_loader(username)

            # automatically sets logged in session cookie
            login_user(user)
            flask.flash('Logged in successfully.')
            next = flask.request.args.get('next')

            # is_safe_url should check if the url is safe for redirects.
            # See http://flask.pocoo.org/snippets/62/ for an example.
            if False and not is_safe_url(next):
                return flask.abort(400)

            return flask.redirect(next or flask.url_for('index')) # next or flask.url_for('index')
    return render_template('./login.html', form=form)

@app.route("/logout")
@login_required
def logout():
    logout_user()
    return flask.redirect('/')

@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'favicon.png', mimetype='image/png')

def pygmentize(text):
    if not hasattr(tls, 'formatter'):
        tls.formatter = HtmlFormatter(nowrap = True)
    if not hasattr(tls, 'lexer'):
        tls.lexer = SqlLexer()
        tls.lexer.add_filter(NameHighlightFilter(names=['GLOB'], tokentype=token.Keyword))
        tls.lexer.add_filter(NameHighlightFilter(names=['text'], tokentype=token.Name))
        tls.lexer.add_filter(KeywordCaseFilter(case='upper'))
    return f'<span class="highlight">{highlight(text, tls.lexer, tls.formatter)}</span>'

@app.get('/search')
def search():
    query = request.args.get('q') or request.form.get('q') or '*'

    legalQuery = legalMessage(query)
    if (legalQuery.getMessage() == ""):
        legalQ = "*"
        print(legalQuery.getError())
    else:
        legalQ = legalQuery.getMessage()

    stmt = f"SELECT * FROM messages WHERE (sender LIKE '{current_user.id}' OR receiver LIKE '{current_user.id}') AND message GLOB '{legalQ}'"

    result = f"Query: {pygmentize(stmt)}\n"
    try:
        c = conn.execute(stmt)
        rows = c.fetchall()
        result = result + 'Result:\n'
        for row in rows:
            result = f'{result}    {dumps(row)}\n'
        c.close()
        return result
    except Error as e:
        return (f'{result}ERROR: {e}', 500)

@app.route('/send', methods=['POST','GET'])
def send():
    try:
        receiver = request.args.get('sender') or request.form.get('sender')
        sender = current_user.id
        message = request.args.get('message') or request.args.get('message')

        legalMsg = legalMessage(message)
        if (legalMsg.getMessage() == ""):
            print(legalMsg.getError())
        else:
            message = legalMsg.getMessage()

        legalRcv = legalMessage(receiver)
        if (legalRcv.getMessage() == ""):
            print(legalRcv.getError())
        else:
            receiver = legalRcv.getMessage()

        if (receiver == "" or message == ""):
            return f'ERROR: missing sender or message'
        stmt = f"INSERT INTO messages (sender, receiver, message) values ('{sender}', '{receiver}', '{message}');"
        result = f"Query: {pygmentize(stmt)}\n"
        conn.execute(stmt)
        return f'{result}ok'
    except Error as e:
        return f'{result}ERROR: {e}'

@app.get('/announcements')
def announcements():
    try:
        stmt = f"SELECT author,text FROM announcements;"
        c = conn.execute(stmt)
        anns = []
        for row in c:
            anns.append({'sender':escape(row[0]), 'message':escape(row[1])})
        return {'data':anns}
    except Error as e:
        return {'error': f'{e}'}

@app.get('/coffee/')
def nocoffee():
    abort(418)

@app.route('/coffee/', methods=['POST','PUT'])
def gotcoffee():
    return "Thanks!"

@app.get('/highlight.css')
def highlightStyle():
    resp = make_response(cssData)
    resp.content_type = 'text/css'
    return resp

try:
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY,
        sender TEXT NOT NULL,
        receiver TEXT NOT NULL,
        message TEXT NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS passwords (
        id integer PRIMARY KEY,
        user TEXT NOT NULL,
        password TEXT NOT NULL,
        token TEXT NOT NULL);''')

        #If we reset the database, uncomment these lines:
    #c.execute("INSERT INTO passwords VALUES (?, ?, ?, ?)", (1, 'alice', 'eda1bcf1aa6aaec2492c55b597268066', 'tiktok'))
    #c.execute("INSERT INTO passwords VALUES (?, ?, ?, ?)", (2, 'bob', 'd5e91a08d715605399cab8ad811446ff', 'cake'))

    c.execute('''CREATE TABLE IF NOT EXISTS announcements (
        id integer PRIMARY KEY,
        author TEXT NOT NULL,
        text TEXT NOT NULL);''')
except Error as e:
    print(e)
    sys.exit(1)
