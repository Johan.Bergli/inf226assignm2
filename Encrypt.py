import hashlib

#Encrypts passwords
def encryption(userName, userPassword):
    psw = userPassword + userName
    hashed = hashlib.md5(psw.encode())
    return hashed.hexdigest()
