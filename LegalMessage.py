from LegalChars import *

class legalMessage:
    message = ""
    error = ""
    def __init__(self, msg):
        if msg == None:
            print("Error Error: input is None")
            self.message = ""
            return
        i = 0
        for char in msg:
            if i > 100:
                self.error = "Error Error: buffer overflow attack!" #Lets just jump to conclusion
                self.message = ""
                return
            elif (char == "'" or char == ";"):
                self.error = "Error Error: SQL injection!"
                self.message = ""
                return
            elif char in legalChars:
                self.message = self.message + char
                i += 1
            else:
                self.error = "Error Error: we're being hacked!" #Lets just jump to conclusion
                self.message = ""
                return

    def getMessage(self):
        return self.message

    def getError(self):
        return self.error
